;; here be PACKAGES
;; package paths and default dir
 ;; uncomment the following two lines for work machine
;; (setq default-directory "D:/vagrant" )
;; (add-to-list 'load-path "D:/applications/emacs/myplugins")
;; uncomment the followign two lines for home machine
(setq default-directory "~/dev" )
(add-to-list 'load-path "~/Ddrive/applications/emacs/myplugins")
(add-to-list 'load-path "~/.emacs.d/elpa")
;; (require 'web-mode)
;; ;; various weirdness for web-mode
;; (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
;; ;;
(require 'undo-tree)
(global-undo-tree-mode)
(require 'nav)
(require 'ido)
(require 'package)
(add-to-list 'package-archives
             '("elpy" . "http://jorgenschaefer.github.io/packages/"))
(package-initialize)


;;FUNCTION definitions here be
;; for commenting and uncommenting lines
(defun my-put-file-name-on-clipboard ()
  "Put the current file name on the clipboard"
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (with-temp-buffer
        (insert filename)
        (clipboard-kill-region (point-min) (point-max)))
      (message filename))))

(defun comment-or-uncomment-region-or-line ()
    "Comments or uncomments the region or the current line if there's no active region."
    (interactive)
    (let (beg end)
        (if (region-active-p)
            (setq beg (region-beginning) end (region-end))
            (setq beg (line-beginning-position) end (line-end-position)))
        (comment-or-uncomment-region beg end)))

;; line-wraping
(when (fboundp 'adaptive-wrap-prefix-mode)
  (defun my-activate-adaptive-wrap-prefix-mode ()
    "Toggle `visual-line-mode' and `adaptive-wrap-prefix-mode' simultaneously."
    (adaptive-wrap-prefix-mode (if visual-line-mode 1 -1)))
  (add-hook 'visual-line-mode-hook 'my-activate-adaptive-wrap-prefix-mode))
(add-hook 'isearch-mode-end-hook 'my-goto-match-beginning)

 ;; something about the search function 
(defun my-goto-match-beginning ()
    (when isearch-forward (goto-char isearch-other-end)))
(add-hook 'isearch-mode-end-hook 'my-goto-match-beginning)

;; macro:
(fset 'end-of-line-then-new-line
   "\C-e\C-j")

;; various MODES go here
;; modify default dir for emacs file backups
(setq backup-directory-alist `(("." . "~/.emacs.d/saves")))

;; python tools
(elpy-enable)

;; easy window-jumping
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;; disable tool-bar
(tool-bar-mode -1)

;; better C-x C-f
(ido-mode t)

;; start emacs server
(server-start)

;; when closing emacs saves the layout and open buffers
(desktop-save-mode 1)

;; shows matching brackets
(show-paren-mode 1) 

;; sharing clipboard with the window manager
(setq x-select-enable-clipboard t)
(setq save-interprogram-paste-before-kill t)

;; for saving window layouts
(autoload 'save-current-configuration "revive" "Save status" t)
(autoload 'resume "revive" "Resume Emacs" t)
(autoload 'wipe "revive" "Wipe Emacs" t)

;; C-x Shift-s to save current window layout
;; C-x Shift-f to return to saved window layout
;; ALSO: You can have separate saved window layouts:
;; C-u <nr> C-x Shift-s
;; And then
;; C-u <nr> C-x Shift-f
(define-key ctl-x-map "S" 'save-current-configuration)
(define-key ctl-x-map "F" 'resume)
(define-key ctl-x-map "K" 'wipe) 
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "C-t") 'set-mark-command)
(global-set-key (kbd "C-'") 'comment-or-uncomment-region-or-line)
(global-set-key [f8] 'nav-toggle)

;; windows-like keybindings: C-c C-v C-x
(cua-mode)

;; this needs to go AFTER the cua-mode
(global-set-key (kbd "C-SPC") 'hippie-expand)

;; C-j should function as RET in Python Elpy mode
;; commented out on Linux. doesn't work. default behaviour is ok
;; (eval-after-load 'elpy
;;   '(define-key elpy-mode-map (kbd "C-j") 'newline))

;; copy full path to file to clipboard
(global-set-key (kbd "M-c") 'my-put-file-name-on-clipboard)

;; add Shift-TAB to run other-window
(global-set-key (kbd "<C-tab>") 'other-window)
;; uncomment the following line for home machine
(global-set-key (kbd "<S-iso-lefttab>") 'other-window)
;; uncomment the following line for work machine
;; (global-set-key (kbd "<backtab>") 'other-window)

;; keybinding for my macro
(global-set-key (kbd "C-o") 'end-of-line-then-new-line)

;; keybindings for TAB and S-TAB for indentation
(global-set-key (kbd "M--") 'elpy-nav-move-iblock-left)
(global-set-key (kbd "M-=") 'elpy-nav-move-iblock-right)

;; keybindings for scrolling
(global-set-key (kbd "C-M-n") 'scroll-up-command)
(global-set-key (kbd "C-M-p") 'scroll-down-command)

;; keybindings for goto-line
(global-set-key (kbd "M-g") 'goto-line)

;; keybinding for iso date stamp insert
(global-set-key (kbd "C-c .") 'org-time-stamp)
